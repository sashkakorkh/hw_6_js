// Exercise 1
//1. Екранування потрібно, щоб у випадку коли  нам потрібно використати в змінних символи, які використовуються в синтаксисі JS,
// ми екрануємо їх за допомогою "/" щоб рушій JS не читав їх як частину коду /*

// 2. Function declaration: function name () {};
//     Function Expression: const name = function () {};
//     Arrow Function:  const name = () => {};
//     Anonymous function function () {};
//
//     3.  При читанні коду, JS переміщує функцію в пам'ять, тому вона буде доступною в будь-якому місці коду.
//     При об'явленні функції  Function declaration її можна викликати як до об'явлення самої функції так і після
//     завдяки тому, щоб вона міститься в пам'яті. Але  Function Expression так не працює, бо змінні в пам'ять не поміщаються */

/*Exercise 2*/

let userFirstName;
let userLastName;
let userDateOfBirth;
let newUser;
function createNewUser () {
    checkUserInfo();
    newUser = {

        getAge () {
            const ageInMs = Date.now() - userDateOfBirth.getTime();
            const ageInMsToYear = new Date(ageInMs);
            return Math.abs(ageInMsToYear.getUTCFullYear() - 1970);
        },
        getLogin() {
            return this['first name'][0].toLowerCase() + this['last name'].toLowerCase()
        },
        getPassword() {
            return this['first name'][0].toUpperCase() + this['last name'].toLowerCase() + this['date of birth'].getUTCFullYear();
        },
    };
    newUser['first name'] = userFirstName;
    newUser['last name'] = userLastName;
    newUser['date of birth'] = userDateOfBirth;
    return newUser;
}


function checkUserInfo () {
    userFirstName = prompt('Enter your name');
    while (!userFirstName || !isNaN(userFirstName) || (!/\S/.test(userFirstName))) {
        userFirstName = prompt('Enter your name correctly');
    }
    userLastName = prompt('Enter your last name');
    while (!userLastName || !isNaN(userLastName) || (!/\S/.test(userLastName))) {
        userLastName = prompt('Enter your last name correctly');
    }
    userDateOfBirth = prompt('Enter your date of birth', 'dd.mm.yyyy');
    while (!userDateOfBirth || (!/\S/.test(userDateOfBirth)) || (!/\d{2}[.]\d{2}[.][1-2]{1}\d{3}/g.test(userDateOfBirth))) {
        userDateOfBirth = prompt('Enter your date of birth correctly');
    }
    isDateValidate();
    userDateForDateConstructor()
}

function isDateValidate () {
    let startYear = 1600
    while (+userDateOfBirth.slice(6, 10) < startYear || new Date(userDateOfBirth.slice(6, 10)).getTime() > Date.now()) {
        userDateOfBirth = prompt('The year is wrong, enter your date correctly');
    }
    while (userDateOfBirth.slice(0,2) === '00' || +userDateOfBirth.slice(0,2) > 31) {
        userDateOfBirth = prompt(`There is no such day, enter your date correctly,that\`s was wrong ${userDateOfBirth}`);
    }
    while (userDateOfBirth.slice(3, 5) === '00' || +userDateOfBirth.slice(3, 5) > 12) {
        userDateOfBirth = prompt(`There is no such month, enter your date correctly,that\'s was wrong ${userDateOfBirth}`);
    }

}

function userDateForDateConstructor () {
    let day = userDateOfBirth.substring(0, 2);
    let month = userDateOfBirth.substring(3, 5);
    let year = userDateOfBirth.substring(6, 10);
    userDateOfBirth = month.concat('.', day, '.', year);
    userDateOfBirth = new Date(userDateOfBirth);
}

console.log(createNewUser());
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());



